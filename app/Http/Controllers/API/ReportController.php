<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    //
    public function getCustomerReport(Request $request)
    {
        //city, month
        try {
            $query = User::selectRaw('users.name as user_name, SUM(items.total_price) as total, COUNT(DISTINCT orders.id) as count, cities.name as city_name')
                ->join('orders', 'users.id', '=', 'orders.user_id')
                ->join('items', 'orders.id', '=', 'items.order_id')
                ->join('cities', 'cities.id', '=', 'users.city_id');

            if ($request->filled('city')) {
                $city = City::where('name', $request->city)->first();
                if($city == null){
                    return response([
                        'data' => [],
                        'code' => -2,
                        'message' => 'City Not Found'
                    ]);
                }
                $query = $query->where('users.city_id', '=', $city->id);
            }

            if ($request->month != 'all') {
                $query = $query->whereMonth('orders.created_at', $request->month);;
            }

            $query = $query->groupBy('user_id');

            $totalOfOrders = $query->get();

            return response([
                'data' => $totalOfOrders,
                'code' => 1,
                'message' => 'Customer Report Retrieved Succefully'
            ]);
        } catch (\Throwable $th) {
            return response([
                'code' => -1,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function getCityReport(Request $request)
    {
        //city, month
        try {
            $query = City::selectRaw('cities.name as city_name, SUM(items.total_price) as total, COUNT(DISTINCT orders.id) as count, SUM(items.quantity) as items_number')
                ->leftJoin('orders', 'cities.id', '=', 'orders.city_id')
                ->leftJoin('items', 'orders.id', '=', 'items.order_id');

            if ($request->filled('city')) {
                $city = City::where('name', $request->city)->first();
                $query = $query->where('orders.city_id', '=', $city->id);
            }

            if ($request->month != 'all') {
                $query = $query->whereMonth('orders.created_at', $request->month);;
            }

            $query = $query->groupBy('city_id');

            $results = $query->get();

            return response([
                'data' => $results,
                'code' => 1,
                'message' => 'City Report Retrieved Succefully'
            ]);
        } catch (\Throwable $th) {
            return response([
                'code' => -1,
                'message' => $th->getMessage(),
            ]);
        }
    }
}
