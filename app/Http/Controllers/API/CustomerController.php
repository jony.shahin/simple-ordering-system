<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //name, phone, email, password, city_id

        try {
            $validateCustomer = Validator::make(
                $request->all(),
                [
                    'name' => 'required|string|max:255',
                    'email' => 'required|email|unique:users,email',
                    'phone' => 'required|string|max:20|unique:users,phone',
                    'password' => ['required', Password::min(8)->uncompromised()],
                    'city_id' => 'required|exists:cities,id',
                ]
            );

            if ($validateCustomer->fails()) {
                return response()->json([
                    'code' => -2,
                    'message' => $validateCustomer->errors(),
                ], 401);
            }

            $customer = User::create([
                'name' => $request->input('name'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->password),
                'city_id' => $request->input('city_id'),
            ]);

            $customer->city;

            return response([
                'data' => $customer,
                'code' => 1,
                'message' => 'Customer Created Succefully',
                'token' => $customer->createToken('simpleOrderingSystem')->plainTextToken,
            ]);
        } catch (\Throwable $th) {
            return response([
                'code' => -1,
                'message' => $th->getMessage(),
            ]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
