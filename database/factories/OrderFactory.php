<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $date = fake()->dateTimeBetween();
        return [
            'order_number' => fake()->unique()->regexify('[A-Z]{4}[0-4]{4}'),
            'created_at' => $date,
            'updated_at' => $date,
        ];
    }
}
