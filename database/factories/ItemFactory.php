<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Item>
 */
class ItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $quantity = fake()->randomDigitNotZero();
        $unit_price = fake()->randomFloat(2,0.5,30);
        $total_price = $quantity * $unit_price;

        return [
            'name' => fake()->name(),
            'quantity' => $quantity,
            'unit_price' => $unit_price,
            'total_price' => $total_price
        ];
    }
}
