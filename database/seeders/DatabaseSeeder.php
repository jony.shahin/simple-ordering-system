<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\City;
use App\Models\Item;
use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // Seed cities
        City::factory()->count(20)->create();

        // Seed users
        User::factory()->count(50)->create([
            'city_id' => function () {
                return City::inRandomOrder()->first()->id;
            },
        ]);

        // Seed orders
        Order::factory()->count(200)->create([
            'user_id' => function () {
                return User::inRandomOrder()->first()->id;
            },
            'city_id' => function () {
                return City::inRandomOrder()->first()->id;
            },
        ]);

        // Seed items
        Item::factory()->count(1000)->create([
            'order_id' => function () {
                return Order::inRandomOrder()->first()->id;
            },
        ]);
    }
}
